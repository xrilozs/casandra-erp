let OPERATOR_ID

$(document).ready(function(){
  getLokasi()

  //data table
  let operator_table = $('#operator-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${OPERATOR_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'username'
                  break
              case 1:
                  newObj.order_by = 'role'
                  break
              case 2:
                  newObj.order_by = 'lokasi'
                  break
              default:
                  newObj.order_by = 'username'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "username"
          },
          { 
            data: "role",
          },
          { 
            data: "operator_lokasi",
            render: function (data, type, row, meta) {
              let lokasi_html = ``
              data.forEach(item => {
                let badge = `<span class="badge badge-primary">${item.lokasi}</span>`
                lokasi_html += badge
              })
              
              return lokasi_html
            }
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-primary operator-update-toggle" data-id="${data}" data-toggle="modal" data-target="#operator-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger operator-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#operator-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
  });

  $('.lokasi-option').select2({
    width: '100%',
    placeholder: "Cari Lokasi..",
    selectOnClose: false,
    closeOnSelect:true,
    multiple: true,
    // dropdownParent: $("#variant-create-modal"),
    ajax: {
      url: `${LOKASI_API_URL}`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        var query = {
          search: params.term,
          page_size: 10,
          page_number: 0,
          ordering: "ASC",
          order_by: "lokasi",
        }
  
        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (response) {
        let menu = response.data
        menu = menu.map(item => {
          item.text = item.lokasi
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: menu
        };
      }
    }
  });
  
  //toggle
  $('#operator-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".operator-update-toggle", "click", function(e) {
    OPERATOR_ID = $(this).data('id')
    $('#operator-update-overlay').show()

    $.ajax({
        async: true,
        url: `${OPERATOR_API_URL}/by-id/${OPERATOR_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#operator-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#operator-update-overlay').hide()
        }
    });
  })

  $("body").delegate(".operator-delete-toggle", "click", function(e) {
    OPERATOR_ID = $(this).data('id')
  })

  //form
  $('#operator-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#operator-create-button')
    
    let $form = $(this),
        request = {
          username: $form.find( "input[name='username']" ).val(),
          password: $form.find( "input[name='password']" ).val(),
          role: $('#operator-role-create-option').find(":selected").val(),
          lokasi: $('#operator-lokasi-create-option').val(),
        }
        
    $.ajax({
        async: true,
        url: OPERATOR_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#operator-create-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#operator-create-button', 'Submit')
          $('#operator-create-modal').modal('hide')
          operator_table.ajax.reload()
        }
    });
  })
  
  $('#operator-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#operator-update-button')

    let $form = $(this),
        request = {
          id: OPERATOR_ID,
          username: $form.find( "input[name='username']" ).val(),
          role: $('#operator-role-update-option').find(":selected").val(),
          lokasi: $('#operator-lokasi-update-option').val(),
        }

    $.ajax({
        async: true,
        url: OPERATOR_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#operator-update-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#operator-update-button', 'Submit')
          $('#operator-update-modal').modal('toggle')
          operator_table.ajax.reload()
        }
    });
  })

  $('#operator-delete-button').click(function (){
    startLoadingButton('#operator-delete-button')
    
    $.ajax({
        async: true,
        url: `${OPERATOR_API_URL}${OPERATOR_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#operator-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#operator-delete-button', 'Submit')
          $('#operator-delete-modal').modal('toggle')
          operator_table.ajax.reload()
        }
    });
  })
})

function getLokasi(){
  $.ajax({
    async: true,
    url: `${LOKASI_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      renderLokasi(res.data)
    }
});
}

function renderForm(data, type){
  let lokasi = data.operator_lokasi.map(item => item.lokasi_id)
  console.log(lokasi)
  let $form = $(`#operator-${type}-form`)
  $form.find( "input[name='username']" ).val(data.username)
  $(`#operator-role-${type}-option`).val(data.role).change()
  $(`#operator-lokasi-${type}-option`).val(lokasi).change()
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#operator-${type}-form`)
  $form.find( "input[name='username']" ).val("")
  if(type=='create') $form.find( "input[name='password']" ).val("")
}

function renderLokasi(lokasi){
  let lokasi_html = ''
  lokasi.forEach(item => {
    const item_html = `<option value="${item.id}">${item.lokasi}</option>`
    lokasi_html += item_html
  });
  $('.lokasi-option').html(lokasi_html)
}