let JENIS_KONSUMEN_ID

$(document).ready(function(){
  //render datatable
  let jenis_konsumen_table = $('#jenis-konsumen-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: JENIS_KONSUMEN_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'kode'
                  break
              case 1:
                  newObj.order_by = 'nama'
                  break
              case 2:
                  newObj.order_by = 'created_at'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "kode",
          },
          { 
            data: "nama",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary jenis-konsumen-update-toggle" data-id="${data}" data-toggle="modal" data-target="#jenis-konsumen-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger jenis-konsumen-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#jenis-konsumen-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#jenis-konsumen-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".jenis-konsumen-update-toggle", "click", function(e) {
    JENIS_KONSUMEN_ID = $(this).data('id')
    clearForm('update')

    $('#jenis-konsumen-update-overlay').show()
    $.ajax({
        async: true,
        url: `${JENIS_KONSUMEN_API_URL}by-id/${JENIS_KONSUMEN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#jenis-konsumen-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#jenis-konsumen-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".jenis-konsumen-delete-toggle", "click", function(e) {
    JENIS_KONSUMEN_ID = $(this).data('id')
  })

  //submit form
  $('#jenis-konsumen-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#jenis-konsumen-create-button')
    
    let $form = $(this)
    let data = {
      nama:  $form.find( "input[name='nama']" ).val(),
      kode:  $form.find( "input[name='kode']" ).val(),
    }
    $.ajax({
      async: true,
      url: JENIS_KONSUMEN_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#jenis-konsumen-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#jenis-konsumen-create-button', 'Submit')
        showSuccess(res.message)
        $('#jenis-konsumen-create-modal').modal('hide')
        jenis_konsumen_table.ajax.reload()
      }
    });
  })
  
  $('#jenis-konsumen-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#jenis-konsumen-update-button')

    let $form = $(this)
    let data = {
      id: JENIS_KONSUMEN_ID,
      nama: $form.find( "input[name='nama']" ).val(),
      kode:  $form.find( "input[name='kode']" ).val(),
    }

    $.ajax({
        async: true,
        url: JENIS_KONSUMEN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#jenis-konsumen-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#jenis-konsumen-update-button', 'Submit')
          showSuccess(res.message)
          $('#jenis-konsumen-update-modal').modal('toggle')
          jenis_konsumen_table.ajax.reload()
        }
    });
  })

  $('#jenis-konsumen-delete-button').click(function (){
    startLoadingButton('#jenis-konsumen-delete-button')
    
    $.ajax({
        async: true,
        url: `${JENIS_KONSUMEN_API_URL}${JENIS_KONSUMEN_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#jenis-konsumen-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#jenis-konsumen-delete-button', 'Submit')
          $('#jenis-konsumen-delete-modal').modal('toggle')
          jenis_konsumen_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#jenis-konsumen-${type}-form`)
  $form.find( "input[name='nama']" ).val(data.nama)
  $form.find( "input[name='kode']" ).val(data.kode)
}

function clearForm(type){
  let $form = $(`#jenis-konsumen-${type}-form`)
  $form.find( "input[name='nama']" ).val("")
  $form.find( "input[name='kode']" ).val("")
}
