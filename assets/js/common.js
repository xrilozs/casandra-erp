let API_URL = localStorage.getItem("api-url")
let WEB_URL = localStorage.getItem("erp-url")
let OPERATOR_API_URL  = `${API_URL}operator/`
let KATEGORI_API_URL  = `${API_URL}kategori/`
let LOKASI_API_URL    = `${API_URL}lokasi/`
let SUPLIER_API_URL   = `${API_URL}suplier/`
let UNIT_API_URL      = `${API_URL}unit/`
let VARIASI_WARNA_API_URL   = `${API_URL}variasi1/`
let VARIASI_UKURAN_API_URL  = `${API_URL}variasi2/`
let ITEM_API_URL            = `${API_URL}item/`
let PEMBELIAN_API_URL       = `${API_URL}pembelian/`
let STOK_API_URL            = `${API_URL}stok/`
let TRANSFER_MASUK_API_URL  = `${API_URL}transfer_masuk/`
let TRANSFER_KELUAR_API_URL = `${API_URL}transfer_keluar/`
let JENIS_KONSUMEN_API_URL  = `${API_URL}jenis_konsumen/`
let JENIS_TRANSAKSI_API_URL = `${API_URL}jenis_transaksi/`
let KONSUMEN_API_URL        = `${API_URL}konsumen/`
let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION           = localStorage.getItem("user-token")
let REFRESH_SESSION   = localStorage.getItem("user-refresh-token")
let OPERATOR_ROLE     = localStorage.getItem("user-role")
let OPERATOR_USERNAME = localStorage.getItem("user-username")
let RETRY_COUNT       = 0
let OPERATOR_PAGES    = ['Kategory', 'Lokasi', 'Suplier', 'Unit', 'Dashboard', 'Variasi Warna', 'Variasi Ukuran']
let MANAGER_PAGES     = ['Kategory', 'Lokasi', 'Suplier', 'Unit', 'Dashboard', 'Variasi Warna', 'Variasi Ukuran']
let CURRENT_PAGE      = localStorage.getItem("current-page")

$('.rich-text-create').summernote({
  placeholder: 'Type here..',
  height: 300
});
$('.rich-text-update').summernote({
  placeholder: 'Type here..',
  height: 300
});

// $('.select2').select2()

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}login`
  }else{
    checkAuthorityPage()
  }
  
  renderHeader()
  renderSidebar()

  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    orientation: 'bottom'
  }).on('changeDate', function(ev){
    $(this).datepicker('hide');
  });
  if($('.datepicker').val()==""){
    $('.datepicker').datepicker("setDate", new Date());
  }
});

function renderHeader(){
  $('#header-operatorName').html(`${OPERATOR_USERNAME} (${OPERATOR_ROLE})`)
}

function renderSidebar(){
  if(OPERATOR_ROLE == 'owner'){
    $(".hidden-menu").css("display", "block")
  }
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-username", data.username);
  localStorage.setItem("user-role", data.role);
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-username");
  localStorage.removeItem("user-role");
  window.location.href = `${WEB_URL}login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}operator/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  console.log("RETRY: ", RETRY_COUNT)
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function checkAuthorityPage(){
  if(OPERATOR_ROLE != 'owner'){
    let pages = OPERATOR_ROLE == 'manager' ? MANAGER_PAGES : OPERATOR_PAGES
    if(!pages.includes(CURRENT_PAGE)){
      window.location.href = `dashboard`
    }
  }
}

function formatRupiah(angka, prefix){
  var angkaStr = angka.replace(/[^,\d]/g, '').toString(),
      split = angkaStr.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDate(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr = datetimeArr[0]
  let arr = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function formatDateID(date_str=null){

  let day_arr = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
  let month_arr = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

  let datetime = date_str ? new Date(date_str) : new Date(),
      day = day_arr[datetime.getDay()],
      date = datetime.getDate(),
      month = month_arr[datetime.getMonth()],
      year = datetime.getFullYear()

  return `${day}, ${date} ${month} ${year}`
}

function sortText(text){
  return text.length > 20 ? text.slice(0, 20) + ".." : text
}

$("#logout-button").click(function(){    
  removeSession()
})

$("#changePassword-toggle").click(function(e){
  let $form = $("#changePassword-form" )
  $form.find( "input[name='oldPassword']" ).val('')
  $form.find( "input[name='newPassword']" ).val('')
})

$("#changePassword-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#changePassword-button")

  let $form = $( this ),
      oldPassword = $form.find( "input[name='oldPassword']" ).val(),
      newPassword = $form.find( "input[name='newPassword']" ).val()
  
  $.ajax({
      async: true,
      url: `${API_URL}operator/change-password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        old_password: oldPassword, 
        new_password: newPassword
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#changePassword-button', 'Submit')
        let is_retry = retryRequest(response)
        if(is_retry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#changePassword-button', 'Submit')
        $('#changePassword-modal').modal('hide')
      }
  });
})

$(document).on('keydown', '.input-decimal', function(e){
  console.log("input decimal")
  let input = $(this);
  let oldVal = input.val();
  let regex
  let attr = input.attr('pattern')
  if (typeof attr !== 'undefined' && attr !== false) {
    regex = new RegExp(input.attr('pattern'), 'g');
  }else{
    regex = new RegExp(/^\s*-?(\d+(\.\d{1,2})?|\.\d{1,2})\s*$/, 'g')
  }
  
  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).on('change', '.input-currency', function(e){
  var oldVal = $(this).val();
  $(this).val(formatRupiah(oldVal))
});
