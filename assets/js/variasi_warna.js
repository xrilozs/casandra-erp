let WARNA_ID

$(document).ready(function(){
  //render datatable
  let warna_table = $('#warna-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: VARIASI_WARNA_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'id'
                  break
              case 1:
                  newObj.order_by = 'warna'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "id",
          },
          { 
            data: "warna",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary warna-update-toggle" data-id="${data}" data-toggle="modal" data-target="#warna-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger warna-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#warna-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#warna-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".warna-update-toggle", "click", function(e) {
    WARNA_ID = $(this).data('id')
    clearForm('update')

    $('#warna-update-overlay').show()
    $.ajax({
        async: true,
        url: `${VARIASI_WARNA_API_URL}by-id/${WARNA_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#warna-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#warna-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".warna-delete-toggle", "click", function(e) {
    WARNA_ID = $(this).data('id')
  })

  //submit form
  $('#warna-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#warna-create-button')
    
    let $form = $(this)
    let data = {
      warna:  $form.find( "input[name='warna']" ).val(),
    }
    $.ajax({
      async: true,
      url: VARIASI_WARNA_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#warna-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#warna-create-button', 'Submit')
        showSuccess(res.message)
        $('#warna-create-modal').modal('hide')
        warna_table.ajax.reload()
      }
    });
  })
  
  $('#warna-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#warna-update-button')

    let $form = $(this)
    let data = {
      id: WARNA_ID,
      warna: $form.find( "input[name='warna']" ).val(),
    }

    $.ajax({
        async: true,
        url: VARIASI_WARNA_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#warna-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#warna-update-button', 'Submit')
          showSuccess(res.message)
          $('#warna-update-modal').modal('toggle')
          warna_table.ajax.reload()
        }
    });
  })

  $('#warna-delete-button').click(function (){
    startLoadingButton('#warna-delete-button')
    
    $.ajax({
        async: true,
        url: `${VARIASI_WARNA_API_URL}${WARNA_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#warna-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#warna-delete-button', 'Submit')
          $('#warna-delete-modal').modal('toggle')
          warna_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#warna-${type}-form`)
  $form.find( "input[name='warna']" ).val(data.warna)
}

function clearForm(type){
  let $form = $(`#warna-${type}-form`)
  $form.find( "input[name='warna']" ).val("")
}
