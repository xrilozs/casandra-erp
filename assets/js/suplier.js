let SUPLIER_ID

$(document).ready(function(){
  //render datatable
  let suplier_table = $('#suplier-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: SUPLIER_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'id_suplier'
                  break
              case 1:
                  newObj.order_by = 'nama'
                  break
              case 2:
                  newObj.order_by = 'no_telpon'
                  break
              case 3:
                  newObj.order_by = 'email'
                  break
              case 4:
                  newObj.order_by = 'keterangan'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "id_suplier",
          },
          { 
            data: "nama",
          },
          { 
            data: "no_telpon",
          },
          { 
            data: "email",
          },
          { 
            data: "keterangan",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          {
            data: "created_at",
          },
          {
            data: "id_suplier",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary suplier-update-toggle" data-id="${data}" data-toggle="modal" data-target="#suplier-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger suplier-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#suplier-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#suplier-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".suplier-update-toggle", "click", function(e) {
    SUPLIER_ID = $(this).data('id')
    clearForm('update')

    $('#suplier-update-overlay').show()
    $.ajax({
        async: true,
        url: `${SUPLIER_API_URL}by-id/${SUPLIER_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#suplier-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#suplier-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".suplier-delete-toggle", "click", function(e) {
    SUPLIER_ID = $(this).data('id')
  })

  //submit form
  $('#suplier-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#suplier-create-button')
    
    let $form = $(this)
    let data = {
      nama:  $form.find( "input[name='nama']" ).val(),
      no_telpon:  $form.find( "input[name='no_telpon']" ).val(),
      email:  $form.find( "input[name='email']" ).val(),
      keterangan:  $form.find( "textarea[name='keterangan']" ).val(),
    }
    $.ajax({
      async: true,
      url: SUPLIER_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#suplier-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#suplier-create-button', 'Submit')
        showSuccess(res.message)
        $('#suplier-create-modal').modal('hide')
        suplier_table.ajax.reload()
      }
    });
  })
  
  $('#suplier-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#suplier-update-button')

    let $form = $(this)
    let data = {
      id: SUPLIER_ID,
      nama:  $form.find( "input[name='nama']" ).val(),
      no_telpon:  $form.find( "input[name='no_telpon']" ).val(),
      email:  $form.find( "input[name='email']" ).val(),
      keterangan:  $form.find( "textarea[name='keterangan']" ).val(),
    }

    $.ajax({
        async: true,
        url: SUPLIER_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#suplier-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#suplier-update-button', 'Submit')
          showSuccess(res.message)
          $('#suplier-update-modal').modal('toggle')
          suplier_table.ajax.reload()
        }
    });
  })

  $('#suplier-delete-button').click(function (){
    startLoadingButton('#suplier-delete-button')
    
    $.ajax({
        async: true,
        url: `${SUPLIER_API_URL}${SUPLIER_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#suplier-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#suplier-delete-button', 'Submit')
          $('#suplier-delete-modal').modal('toggle')
          suplier_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#suplier-${type}-form`)
  $form.find( "input[name='nama']" ).val(data.nama)
  $form.find( "input[name='no_telpon']" ).val(data.no_telpon)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "textarea[name='keterangan']" ).val(data.keterangan)
}

function clearForm(type){
  let $form = $(`#suplier-${type}-form`)
  $form.find( "input[name='nama']" ).val("")
  $form.find( "input[name='no_telpon']" ).val("")
  $form.find( "input[name='email']" ).val("")
  $form.find( "textarea[name='keterangan']" ).val("")
}
