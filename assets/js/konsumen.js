let KONSUMEN_ID

$(document).ready(function(){
  get_jenis_konsumen()

  //render datatable
  let konsumen_table = $('#konsumen-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: KONSUMEN_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'username'
                  break
              case 1:
                  newObj.order_by = 'no_telpon'
                  break
              case 2:
                  newObj.order_by = 'email'
                  break
              case 3:
                  newObj.order_by = 'jenis_konsumen'
                  break
              case 4:
                  newObj.order_by = 'created_at'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "username",
          },
          { 
            data: "no_telpon",
          },
          { 
            data: "email",
          },
          {
            data: "jenis_konsumen_nama"
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary konsumen-update-toggle" data-id="${data}" data-toggle="modal" data-target="#konsumen-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger konsumen-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#konsumen-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#konsumen-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".konsumen-update-toggle", "click", function(e) {
    KONSUMEN_ID = $(this).data('id')
    clearForm('update')

    $('#konsumen-update-overlay').show()
    $.ajax({
        async: true,
        url: `${KONSUMEN_API_URL}by-id/${KONSUMEN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#konsumen-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#konsumen-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".konsumen-delete-toggle", "click", function(e) {
    KONSUMEN_ID = $(this).data('id')
  })

  //submit form
  $('#konsumen-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#konsumen-create-button')
    
    let $form = $(this)
    let data = {
      username: $form.find( "input[name='username']" ).val(),
      no_telpon: $form.find( "input[name='no_telpon']" ).val(),
      email: $form.find( "input[name='email']" ).val(),
      dob: $form.find( "input[name='dob']" ).val(),
      no_member: $form.find( "input[name='no_member']" ).val(),
      jenis_konsumen: $('#konsumen-jenis-create-field').find(":selected").val(),
    }
    $.ajax({
      async: true,
      url: KONSUMEN_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#konsumen-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#konsumen-create-button', 'Submit')
        showSuccess(res.message)
        $('#konsumen-create-modal').modal('hide')
        konsumen_table.ajax.reload()
      }
    });
  })
  
  $('#konsumen-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#konsumen-update-button')

    let $form = $(this)
    let data = {
      id: KONSUMEN_ID,
      username: $form.find( "input[name='username']" ).val(),
      no_telpon: $form.find( "input[name='no_telpon']" ).val(),
      email: $form.find( "input[name='email']" ).val(),
      dob: $form.find( "input[name='dob']" ).val(),
      no_member: $form.find( "input[name='no_member']" ).val(),
      jenis_konsumen: $('#konsumen-jenis-create-field').find(":selected").val(),
    }

    $.ajax({
        async: true,
        url: KONSUMEN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#konsumen-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#konsumen-update-button', 'Submit')
          showSuccess(res.message)
          $('#konsumen-update-modal').modal('toggle')
          konsumen_table.ajax.reload()
        }
    });
  })

  $('#konsumen-delete-button').click(function (){
    startLoadingButton('#konsumen-delete-button')
    
    $.ajax({
        async: true,
        url: `${KONSUMEN_API_URL}${KONSUMEN_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#konsumen-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#konsumen-delete-button', 'Submit')
          $('#konsumen-delete-modal').modal('toggle')
          konsumen_table.ajax.reload()
        }
    });
  })
})

function get_jenis_konsumen(){
  $.ajax({
    async: true,
    url: `${JENIS_KONSUMEN_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_jenis_konsumen(res.data)
    }
  });
}

function renderForm(data, type){
  let $form = $(`#konsumen-${type}-form`)
  $form.find( "input[name='username']" ).val(data.username)
  $form.find( "input[name='no_telpon']" ).val(data.no_telpon)
  $form.find( "input[name='email']" ).val(data.email)
  // $form.find( "input[name='dob']" ).val(data.dob)
  $form.find( "input[name='no_member']" ).val(data.no_member)
  $(`#konsumen-jenis-${type}-field`).val(data.jenis_konsumen).change()
  $('.datepicker').datepicker("setDate", new Date(data.dob));

}

function clearForm(type){
  let $form = $(`#konsumen-${type}-form`)
  $form.find( "input[name='username']" ).val("")
  $form.find( "input[name='no_telpon']" ).val("")
  $form.find( "input[name='email']" ).val("")
  $form.find( "input[name='dob']" ).val("")
  $form.find( "input[name='no_member']" ).val("")
}

function render_jenis_konsumen(data){
  let jenis_konsumen_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.kode}">${item.nama}</option>`
    jenis_konsumen_html += item_html
  });
  $('.jenis-konsumen-option').html(jenis_konsumen_html)
}