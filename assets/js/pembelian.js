let PEMBELIAN_ID,
    SELECTED_ITEM,
    PEMBELIAN_ITEMS = []

$(document).ready(function(){
  get_suplier()
  get_lokasi()

  //render datatable
  let pembelian_table = $('#pembelian-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: PEMBELIAN_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'no_faktur'
                  break
              case 1:
                  newObj.order_by = 'nama_barang'
                  break
              case 2:
                  newObj.order_by = 'harga'
                  break
              case 3:
                  newObj.order_by = 'qty'
                  break
              case 4:
                  newObj.order_by = 'diskon'
                  break
              case 5:
                  newObj.order_by = 'ppn'
                  break
              case 6:
                  newObj.order_by = 'lokasi'
                  break
              case 7:
                  newObj.order_by = 'nama_suplier'
                  break
              case 8:
                newObj.order_by = 'created_at'
                break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "desc" ]],
      columns: [
          { 
            data: "no_faktur",
          },
          { 
            data: "nama_barang",
          },
          { 
            data: "harga",
            render: function (data, type, row, meta) {
              return data ? formatRupiah(data, true) : "-"
            }
          },
          { 
            data: "qty",
          },
          { 
            data: "diskon",
            render: function (data, type, row, meta) {
              return data ? formatRupiah(data, true) : "-"
            }
          },
          { 
            data: "ppn",
            render: function (data, type, row, meta) {
              return data ? formatRupiah(data, true) : "-"
            }
          },
          { 
            data: "lokasi",
          },
          { 
            data: "nama_suplier",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary pembelian-detail-toggle" data-id="${data}" data-toggle="modal" data-target="#pembelian-detail-modal" title="detail">
                <i class="fas fa-search"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });

  $('#pembelian-detail-item-create-field').select2({
    width: '100%',
    placeholder: "Cari Item..",
    selectOnClose: false,
    closeOnSelect:true,
    dropdownParent: $("#pembelian-create-modal"),
    ajax: {
      url: `${ITEM_API_URL}`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        var query = {
          search: params.term,
          page_size: 10,
          page_number: 0,
          ordering: "ASC",
          order_by: "nama_barang",
          type: "Variasi"
        }
  
        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (response) {
        let menu = response.data
        menu = menu.map(item => {
          item.text = `(${item.kode}) ${item.nama_barang}`
          item.id = item.kode
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: menu
        };
      }
    }
  });
  $('#pembelian-detail-item-create-field').data('select2').$selection.css('height', '50px');
  $('#pembelian-detail-item-create-field').on("select2:selecting", function(e) {
    let item = e.params.args.data
    console.log("ITEM ", item)
    SELECTED_ITEM = item
    $('#pembelian-detail-item-button').attr("disabled", false)
  })
  
  //button action click
  $("#pembelian-create-toggle").click(function(e) {
    clearForm('create')
  })

  $('#pembelian-detail-item-button').click(function(){
    let item = SELECTED_ITEM
    item.qty = 0;
    item.harga = 0;
    item.ppn = 0;
    item.diskon = 0;
    item.harga_pokok = 0;

    PEMBELIAN_ITEMS.push(item)
    renderTable()
  })

  $("body").delegate(".pembelian-detail-delete-button", "click", function(e) {
    let id = $(this).data("id")
    PEMBELIAN_ITEMS.splice(id, 1)
    renderTable()
  })

  
  $("body").delegate(".pembelian-detail-toggle", "click", function(e) {
    PEMBELIAN_ID = $(this).data('id')

    $('#pembelian-detail-overlay').show()
    $.ajax({
        async: true,
        url: `${PEMBELIAN_API_URL}by-id/${PEMBELIAN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#pembelian-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#pembelian-detail-overlay').hide()
          renderForm(response)
        }
    });
  })

  //submit form
  $('#pembelian-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#pembelian-create-button')
    if(PEMBELIAN_ITEMS.length <= 0){
      showError("Item tidak boleh kosong")
      endLoadingButton('#pembelian-create-button', 'Submit')
      return
    }

    let isItemValid = true
    let items = []
    for(let i=0; i<PEMBELIAN_ITEMS.length; i++){
      let item = {
        kode_barang: $(`#pembelian-detail-kode-${i}`).val(),
        harga: $(`#pembelian-detail-harga-${i}`).val(),
        qty: $(`#pembelian-detail-qty-${i}`).val(),
        harga_pokok: $(`#pembelian-detail-pokok-${i}`).val(),
        ppn: $(`#pembelian-detail-ppn-${i}`).val(), 
        diskon: $(`#pembelian-detail-diskon-${i}`).val()
      }
      
      if(item.harga && item.qty && item.harga_pokok && parseInt(item.harga) && parseInt(item.harga_pokok) && parseInt(item.qty)){
        items.push(item)
      }else{
        isItemValid = false
        break
      }
    }
    console.log(items)

    if(!isItemValid){
      showError("Data Pembelian Item harus diisi")
      endLoadingButton('#pembelian-create-button', 'Submit')
      return
    }
    
    let $form = $(this)
    let data = {
      no_faktur:  $form.find( "input[name='no_faktur']" ).val(),
      keterangan:  $form.find( "textarea[name='keterangan']" ).val(),
      suplier_id:  $('#pembelian-suplier-create-field').find(":selected").val(),
      lokasi_id:  $('#pembelian-lokasi-create-field').find(":selected").val(),
      items: items
    }
    $.ajax({
      async: true,
      url: PEMBELIAN_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#pembelian-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#pembelian-create-button', 'Submit')
        showSuccess(res.message)
        $('#pembelian-create-modal').modal('hide')
        pembelian_table.ajax.reload()
      }
    });
  })
})

function get_suplier(){
  $.ajax({
    async: true,
    url: `${SUPLIER_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_suplier(res.data)
    }
  });
}

function get_lokasi(){
  $.ajax({
    async: true,
    url: `${LOKASI_API_URL}list`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_lokasi(res.data)
    }
  });
}

function renderTable(){
  let rows_html = ''
  if(PEMBELIAN_ITEMS.length > 0){
    PEMBELIAN_ITEMS.forEach((item, i) => {
      const row_html = `<tr>
        <td><input type="text" id="pembelian-detail-kode-${i}" class="form-control" value="${item.kode}" readonly></td>
        <td>${item.nama_barang}</td>
        <td><input type="number" id="pembelian-detail-qty-${i}" class="form-control" value="${item.qty}" required></td>
        <td><input type="number" id="pembelian-detail-harga-${i}" class="form-control" value="${item.harga}" required></td>
        <td><input type="number" id="pembelian-detail-ppn-${i}" class="form-control" value="${item.ppn}"></td>
        <td><input type="number" id="pembelian-detail-diskon-${i}" class="form-control" value="${item.diskon}"></td>
        <td><input type="number" id="pembelian-detail-pokok-${i}" class="form-control" value="${item.harga_pokok}" required></td>
        <td><button class="btn btn-danger pembelian-detail-delete-button" data-id="${i}"><i class="fas fa-trash" /></button></td>
      </tr>`
      rows_html += row_html
    })
  }else{
    rows_html = `<tr>
      <td colspan="8"><p align="center">Belum ada item</p></td>
    </tr>`
  }

  $('#pembelian-detail-row').html(rows_html)
}

function renderForm(data){
  let $form = $(`#pembelian-detail-form`)
  $form.find( "input[name='no_faktur']" ).val(data.no_faktur)
  $form.find( "input[name='kode']" ).val(data.kode_barang)
  $form.find( "input[name='model']" ).val(data.nama_barang)
  $form.find( "input[name='harga']" ).val(formatRupiah(data.harga, true))
  $form.find( "input[name='qty']" ).val(data.qty)
  $form.find( "input[name='diskon']" ).val(data.diskon ? formatRupiah(data.diskon, true) : "-" )
  $form.find( "input[name='ppn']" ).val(data.ppn ? formatRupiah(data.ppn, true) : "-")
  $form.find( "input[name='harga_pokok']" ).val(formatRupiah(data.harga_pokok, true))
  $form.find( "input[name='tanggal']" ).val(data.tanggal)
  $form.find( "input[name='suplier']" ).val(data.nama_suplier)
  $form.find( "input[name='lokasi']" ).val(data.lokasi)
  $form.find( "textarea[name='keterangan']" ).val(data.keterangan)
}

function clearForm(type){
  let $form = $(`#pembelian-${type}-form`)
  $form.find( "input[name='nama']" ).val("")
  $form.find( "input[name='simbol']" ).val("")
}

function render_suplier(data){
  let suplier_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.id_suplier}">${item.nama}</option>`
    suplier_html += item_html
  });
  $('#pembelian-suplier-create-field').html(suplier_html)
}

function render_lokasi(data){
  let lokasi_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.id}">${item.lokasi}</option>`
    lokasi_html += item_html
  });
  $('#pembelian-lokasi-create-field').html(lokasi_html)
}