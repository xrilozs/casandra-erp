let KATEGORI_ID

$(document).ready(function(){
  //render datatable
  let kategori_table = $('#kategori-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: KATEGORI_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'kode'
                  break
              case 1:
                  newObj.order_by = 'kategori'
                  break
              case 2:
                  newObj.order_by = 'kode_ginee'
                  break
              default:
                  newObj.order_by = 'kode'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "kode",
          },
          { 
            data: "kategori",
          },
          { 
            data: "kode_ginee",
          },
          {
            data: "created_at",
          },
          {
            data: "kode",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary kategori-update-toggle" data-id="${data}" data-toggle="modal" data-target="#kategori-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger kategori-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#kategori-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#kategori-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".kategori-update-toggle", "click", function(e) {
    KATEGORI_ID = $(this).data('id')
    clearForm('update')

    $('#kategori-update-overlay').show()
    $.ajax({
        async: true,
        url: `${KATEGORI_API_URL}by-id/${KATEGORI_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#kategori-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#kategori-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".kategori-delete-toggle", "click", function(e) {
    KATEGORI_ID = $(this).data('id')
  })

  //submit form
  $('#kategori-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#kategori-create-button')
    
    let $form = $(this)
    let data = {
      kategori:  $form.find( "input[name='kategori']" ).val(),
      kode_ginee:  $form.find( "input[name='kode_ginee']" ).val(),
    }
    $.ajax({
      async: true,
      url: KATEGORI_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#kategori-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#kategori-create-button', 'Submit')
        showSuccess(res.message)
        $('#kategori-create-modal').modal('hide')
        kategori_table.ajax.reload()
      }
    });
  })
  
  $('#kategori-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#kategori-update-button')

    let $form = $(this)
    let data = {
      id: KATEGORI_ID,
      kategori: $form.find( "input[name='kategori']" ).val(),
      kode_ginee: $form.find( "input[name='kode_ginee']" ).val(),
    }

    $.ajax({
        async: true,
        url: KATEGORI_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kategori-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#kategori-update-button', 'Submit')
          showSuccess(res.message)
          $('#kategori-update-modal').modal('toggle')
          kategori_table.ajax.reload()
        }
    });
  })

  $('#kategori-delete-button').click(function (){
    startLoadingButton('#kategori-delete-button')
    
    $.ajax({
        async: true,
        url: `${KATEGORI_API_URL}${KATEGORI_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#kategori-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#kategori-delete-button', 'Submit')
          $('#kategori-delete-modal').modal('toggle')
          kategori_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#kategori-${type}-form`)
  $form.find( "input[name='kategori']" ).val(data.kategori)
  $form.find( "input[name='kode_ginee']" ).val(data.kode_ginee)
}

function clearForm(type){
  let $form = $(`#kategori-${type}-form`)
  $form.find( "input[name='kategori']" ).val("")
  $form.find( "input[name='kode_ginee']" ).val("")
}
