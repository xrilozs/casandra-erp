let API_URL = localStorage.getItem("api-url")
let WEB_URL = localStorage.getItem("erp-url")
let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION     = localStorage.getItem("user-token");
let ADMIN_ROLE  = localStorage.getItem("user-role");

$(document).ready(function(){
  if(SESSION){
    getProfile()
  }

  $("#login-form").submit(function(e) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form     = $( this ),
        username  = $form.find( "input[name='username']" ).val(),
        password  = $form.find( "input[name='password']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}operator/login`,
        type: 'POST',
        data: JSON.stringify({
          username: username,
          password: password
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Masuk')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          window.location.href = 'dashboard'
        }
    });    
  })
})

function getProfile(){
  $.ajax({
    async: true,
    url: `${API_URL}operator/profile`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      response = res.responseJSON
      removeSession()
    },
    success: function(res) {
      response = res.data;
      setSession(response)
      window.location.href = 'dashboard'
    }
}); 
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-username", data.username);
  localStorage.setItem("user-role", data.role);
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-username");
  localStorage.removeItem("user-role");
  window.location.href = `${WEB_URL}login`
}