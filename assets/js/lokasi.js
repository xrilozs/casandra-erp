let LOKASI_ID

$(document).ready(function(){
  //render datatable
  let lokasi_table = $('#lokasi-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: LOKASI_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'id'
                  break
              case 1:
                  newObj.order_by = 'lokasi'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "id",
          },
          { 
            data: "lokasi",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary lokasi-update-toggle" data-id="${data}" data-toggle="modal" data-target="#lokasi-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger lokasi-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#lokasi-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#lokasi-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".lokasi-update-toggle", "click", function(e) {
    LOKASI_ID = $(this).data('id')
    clearForm('update')

    $('#lokasi-update-overlay').show()
    $.ajax({
        async: true,
        url: `${LOKASI_API_URL}by-id/${LOKASI_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#lokasi-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#lokasi-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".lokasi-delete-toggle", "click", function(e) {
    LOKASI_ID = $(this).data('id')
  })

  //submit form
  $('#lokasi-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#lokasi-create-button')
    
    let $form = $(this)
    let data = {
      lokasi:  $form.find( "input[name='lokasi']" ).val(),
    }
    $.ajax({
      async: true,
      url: LOKASI_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#lokasi-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#lokasi-create-button', 'Submit')
        showSuccess(res.message)
        $('#lokasi-create-modal').modal('hide')
        lokasi_table.ajax.reload()
      }
    });
  })
  
  $('#lokasi-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#lokasi-update-button')

    let $form = $(this)
    let data = {
      id: LOKASI_ID,
      lokasi: $form.find( "input[name='lokasi']" ).val(),
    }

    $.ajax({
        async: true,
        url: LOKASI_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#lokasi-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#lokasi-update-button', 'Submit')
          showSuccess(res.message)
          $('#lokasi-update-modal').modal('toggle')
          lokasi_table.ajax.reload()
        }
    });
  })

  $('#lokasi-delete-button').click(function (){
    startLoadingButton('#lokasi-delete-button')
    
    $.ajax({
        async: true,
        url: `${LOKASI_API_URL}${LOKASI_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#lokasi-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#lokasi-delete-button', 'Submit')
          $('#lokasi-delete-modal').modal('toggle')
          lokasi_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#lokasi-${type}-form`)
  $form.find( "input[name='lokasi']" ).val(data.lokasi)
}

function clearForm(type){
  let $form = $(`#lokasi-${type}-form`)
  $form.find( "input[name='lokasi']" ).val("")
}
