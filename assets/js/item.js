let ITEM_ID

$(document).ready(function(){
  get_satuan()
  get_kategori()
  get_suplier()
  get_variasi1()
  get_variasi2()

  //render datatable
  let item_table = $('#item-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: ITEM_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'status'
                  break
              case 1:
                  newObj.order_by = 'kode'
                  break
              case 2:
                  newObj.order_by = 'kode_master'
                  break
              case 3:
                  newObj.order_by = 'nama_barang'
                  break
              case 4:
                  newObj.order_by = 'created_at'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "status",
            render: function (data, type, row, meta) {
              let color = data == 'Master' ? 'primary' : 'success' 
              let badge = `<span class="badge badge-${color}">${data}</span>`
              return badge
            }
          },
          { 
            data: "kode",
          },
          { 
            data: "kode_master",
          },
          {
            data: "nama_barang",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let status = row.status == 'Master' ? 'master' : 'variant'
              let color = row.status == 'Master' ? 'primary' : 'success'
              return `<button class="btn btn-sm btn-${color} ${status}-update-toggle" data-id="${data}" data-toggle="modal" data-target="#${status}-update-modal" title="update ${row.status}">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger item-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#item-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });

  $('#variant-master-create-field').select2({
    width: '100%',
    placeholder: "Cari Master..",
    selectOnClose: false,
    closeOnSelect:true,
    dropdownParent: $("#variant-create-modal"),
    ajax: {
      url: `${ITEM_API_URL}`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        var query = {
          search: params.term,
          page_size: 10,
          page_number: 0,
          ordering: "ASC",
          order_by: "nama_barang",
          type: "Master"
        }
  
        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (response) {
        let menu = response.data
        menu = menu.map(item => {
          item.text = `(${item.kode}) ${item.nama_barang}`
          item.id = item.kode
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: menu
        };
      }
    }
  });
  $('#variant-master-create-field').data('select2').$selection.css('height', '50px');
  $('#variant-master-create-field').on("select2:selecting", function(e) {
    let master = e.params.args.data
    console.log("ITEM ", master)
    $('#variant-satuan-create-field').val(master.satuan.nama)
    $('#variant-kategori-create-field').val(master.kategori.kategori)
    $('#variant-suplier-create-field').val(master.suplier.nama)
  })

  //button action click
  $("#master-create-toggle").click(function(e) {
    clearForm('master','create')
  })
  $("#variant-create-toggle").click(function(e) {
    clearForm('variant','create')
  })
  
  $("body").delegate(".master-update-toggle", "click", function(e) {
    ITEM_ID = $(this).data('id')
    clearForm('master','update')

    $('#master-update-overlay').show()
    $.ajax({
        async: true,
        url: `${ITEM_API_URL}by-id/${ITEM_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#master-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#master-update-overlay').hide()
          renderForm(response, 'master')
        }
    });
  })
  $("body").delegate(".variant-update-toggle", "click", function(e) {
    ITEM_ID = $(this).data('id')
    clearForm('variant','update')

    $('#variant-update-overlay').show()
    $.ajax({
        async: true,
        url: `${ITEM_API_URL}by-id/${ITEM_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#variant-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#variant-update-overlay').hide()
          renderForm(response, 'variant')
        }
    });
  })

  $("body").delegate(".item-delete-toggle", "click", function(e) {
    ITEM_ID = $(this).data('id')
  })

  //submit form
  $('#master-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#master-create-button')
    
    let $form = $(this)
    let data = {
      kode: $form.find( "input[name='kode']" ).val(),
      status: "Master",
      model: $form.find( "input[name='model']" ).val(),
      kode_satuan: $('#master-satuan-create-field').find(":selected").val(),
      kode_kategori: $('#master-kategori-create-field').find(":selected").val(),
      id_suplier: $('#master-suplier-create-field').find(":selected").val(),
      keterangan: $form.find( "textarea[name='keterangan']" ).val(),
    }
    $.ajax({
      async: true,
      url: ITEM_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#master-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#master-create-button', 'Submit')
        showSuccess(res.message)
        $('#master-create-modal').modal('hide')
        item_table.ajax.reload()
      }
    });
  })
  $('#variant-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#variant-create-button')
    
    let $form = $(this)
    let data = {
      kode: $form.find( "input[name='kode']" ).val(),
      kode_master: $('#variant-master-create-field').find(":selected").val(),
      status: "Variasi",
      kode_variasi1: $('#variant-variasi1-create-field').find(":selected").val(),
      kode_variasi2: $('#variant-variasi2-create-field').find(":selected").val(),
      keterangan: $form.find( "textarea[name='keterangan']" ).val(),
    }
    $.ajax({
      async: true,
      url: ITEM_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#variant-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#variant-create-button', 'Submit')
        showSuccess(res.message)
        $('#variant-create-modal').modal('hide')
        item_table.ajax.reload()
      }
    });
  })
  
  $('#master-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#master-update-button')

    let $form = $(this)
    let data = {
      id: ITEM_ID,
      model: $form.find( "input[name='model']" ).val(),
      kode_satuan: $('#master-satuan-update-field').find(":selected").val(),
      kode_kategori: $('#master-kategori-update-field').find(":selected").val(),
      id_suplier: $('#master-suplier-update-field').find(":selected").val(),
      keterangan: $form.find( "textarea[name='keterangan']" ).val(),
    }

    $.ajax({
        async: true,
        url: ITEM_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#master-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#master-update-button', 'Submit')
          showSuccess(res.message)
          $('#master-update-modal').modal('toggle')
          item_table.ajax.reload()
        }
    });
  })
  $('#variant-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#variant-update-button')

    let $form = $(this)
    let data = {
      id: ITEM_ID,
      kode_variasi1: $('#variant-variasi1-update-field').find(":selected").val(),
      kode_variasi2: $('#variant-variasi2-update-field').find(":selected").val(),
      keterangan: $form.find( "textarea[name='keterangan']" ).val(),
    }

    $.ajax({
        async: true,
        url: ITEM_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#variant-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#variant-update-button', 'Submit')
          showSuccess(res.message)
          $('#variant-update-modal').modal('toggle')
          item_table.ajax.reload()
        }
    });
  })

  $('#item-delete-button').click(function (){
    startLoadingButton('#item-delete-button')
    
    $.ajax({
        async: true,
        url: `${ITEM_API_URL}${ITEM_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#item-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#item-delete-button', 'Submit')
          $('#item-delete-modal').modal('toggle')
          item_table.ajax.reload()
        }
    });
  })
})

function get_satuan(){
  $.ajax({
    async: true,
    url: `${UNIT_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_satuan(res.data)
    }
  });
}

function get_kategori(){
  $.ajax({
    async: true,
    url: `${KATEGORI_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_kategori(res.data)
    }
  });
}

function get_suplier(){
  $.ajax({
    async: true,
    url: `${SUPLIER_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_suplier(res.data)
    }
  });
}

function get_variasi1(){
  $.ajax({
    async: true,
    url: `${VARIASI_WARNA_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_variasi1(res.data)
    }
  });
}

function get_variasi2(){
  $.ajax({
    async: true,
    url: `${VARIASI_UKURAN_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_variasi2(res.data)
    }
  });
}

function renderForm(data, type){
  let $form = $(`#${type}-update-form`)
  $form.find( "input[name='kode']" ).val(data.kode)
  $form.find( "textarea[name='keterangan']" ).val(data.keterangan)
  if(type == 'master'){
    $form.find( "input[name='model']" ).val(data.model)
    $(`#${type}-satuan-update-field`).val(data.kode_satuan).change()
    $(`#${type}-kategori-update-field`).val(data.kode_kategori).change()
    $(`#${type}-suplier-update-field`).val(data.id_suplier).change()
  }else{
    $form.find( "input[name='kode_master']" ).val(data.kode_master)
    $form.find( "input[name='kode_satuan']" ).val(data.satuan.nama)
    $form.find( "input[name='kode_kategori']" ).val(data.kategori.kategori)
    $form.find( "input[name='id_suplier']" ).val(data.suplier.nama)

    $(`#${type}-variasi1-update-field`).val(data.kode_variasi1).change()
    $(`#${type}-variasi2-update-field`).val(data.kode_variasi2).change()
  }
}

function clearForm(type, method){
  let $form = $(`#${type}-${method}-form`)
  $form.find( "input[name='kode']" ).val("")
  $form.find( "textarea[name='keterangan']" ).val("")
  if(type == 'master'){
    $form.find( "input[name='model']" ).val("")
  }
}

function render_satuan(data){
  let satuan_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.kode}">${item.nama}</option>`
    satuan_html += item_html
  });
  $('.satuan-option').html(satuan_html)
}

function render_kategori(data){
  let kategori_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.kode}">${item.kategori}</option>`
    kategori_html += item_html
  });
  $('.kategori-option').html(kategori_html)
}

function render_suplier(data){
  let suplier_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.id_suplier}">${item.nama}</option>`
    suplier_html += item_html
  });
  $('.suplier-option').html(suplier_html)
}

function render_variasi1(data){
  let variasi1_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.id}">${item.warna}</option>`
    variasi1_html += item_html
  });
  $('.variasi1-option').html(variasi1_html)
}

function render_variasi2(data){
  let variasi2_html = ``
  data.forEach(item => {
    let item_html = `<option value="${item.id}">${item.ukuran}</option>`
    variasi2_html += item_html
  });
  $('.variasi2-option').html(variasi2_html)
}
