let STOK_ID

$(document).ready(function(){
  //render datatable
  let stok_table = $('#stok-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: STOK_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'kode_barang'
                  break
              case 1:
                  newObj.order_by = 'kode_master'
                  break
              case 2:
                  newObj.order_by = 'nama_barang'
                  break
              case 3:
                  newObj.order_by = 'lokasi'
                  break
              case 4:
                  newObj.order_by = 'suplier'
                  break
              case 5:
                  newObj.order_by = 'kategori'
                  break
              case 6:
                  newObj.order_by = 'unit'
                  break
              case 7:
                  newObj.order_by = 'warna'
                  break
              case 8:
                  newObj.order_by = 'ukuran'
                  break
              case 9:
                  newObj.order_by = 'stok'
                  break
              case 10:
                  newObj.order_by = 'harga_beli'
                  break
              case 11:
                  newObj.order_by = 'harga_jual'
                  break
              case 12:
                  newObj.order_by = 'acc'
                  break
              case 13:
                  newObj.order_by = 'created_at'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "desc" ]],
      columns: [
          { 
            data: "kode_barang",
          },
          { 
            data: "kode_master",
          },
          { 
            data: "nama_barang",
          },
          { 
            data: "lokasi",
          },
          { 
            data: "suplier",
          },
          { 
            data: "kategori",
          },
          { 
            data: "unit",
          },
          { 
            data: "warna",
          },
          { 
            data: "ukuran",
          },
          { 
            data: "stok",
          },
          { 
            data: "harga_beli",
            render: function (data, type, row, meta) {
              return data ? formatRupiah(data, true) : "-"
            }
          },
          { 
            data: "harga_jual",
            render: function (data, type, row, meta) {
              return data ? formatRupiah(data, true) : "-"
            }
          },
          { 
            data: "acc",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary stok-detail-toggle" data-id="${data}" data-toggle="modal" data-target="#stok-detail-modal" title="detail">
                <i class="fas fa-search"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });

  
  $("body").delegate(".stok-detail-toggle", "click", function(e) {
    STOK_ID = $(this).data('id')

    $('#stok-detail-overlay').show()
    $.ajax({
        async: true,
        url: `${STOK_API_URL}by-id/${STOK_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#stok-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#stok-detail-overlay').hide()
          renderForm(response)
        }
    });
  })
})

function renderForm(data){
  let $form = $(`#stok-detail-form`)
  $form.find( "input[name='kode_barang']" ).val(data.kode_barang)
  $form.find( "input[name='kode_master']" ).val(data.kode_master)
  $form.find( "input[name='nama_barang']" ).val(data.nama_barang)
  $form.find( "input[name='lokasi']" ).val(data.lokasi)
  $form.find( "input[name='suplier']" ).val(data.suplier)
  $form.find( "input[name='kategori']" ).val(data.kategori)
  $form.find( "input[name='unit']" ).val(data.unit)
  $form.find( "input[name='warna']" ).val(data.warna)
  $form.find( "input[name='ukuran']" ).val(data.ukuran)
  $form.find( "input[name='stok']" ).val(data.stok)
  $form.find( "input[name='rata_beli']" ).val(formatRupiah(data.rata_beli, true))
  $form.find( "input[name='harga_beli']" ).val(formatRupiah(data.harga_beli, true))
  $form.find( "input[name='harga_pokok']" ).val(formatRupiah(data.harga_pokok, true))
  $form.find( "input[name='harga_jual']" ).val(data.harga_jual ? formatRupiah(data.harga_jual, true) : "-")
  $form.find( "input[name='harga_grosir']" ).val(data.harga_grosir ? formatRupiah(data.harga_grosir, true) : "-")
  $form.find( "input[name='harga_retail']" ).val(data.harga_retail ? formatRupiah(data.harga_retail, true) : "-")
  $form.find( "input[name='harga_reseller']" ).val(data.harga_reseller ? formatRupiah(data.harga_reseller, true) : "-")
}
