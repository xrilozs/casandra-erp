let JENIS_TRANSAKSI_ID

$(document).ready(function(){
  //render datatable
  let jenis_transaksi_table = $('#jenis-transaksi-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: JENIS_TRANSAKSI_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'kode'
                  break
              case 1:
                  newObj.order_by = 'nama'
                  break
              case 2:
                  newObj.order_by = 'created_at'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "kode",
          },
          { 
            data: "nama",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary jenis-transaksi-update-toggle" data-id="${data}" data-toggle="modal" data-target="#jenis-transaksi-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger jenis-transaksi-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#jenis-transaksi-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#jenis-transaksi-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".jenis-transaksi-update-toggle", "click", function(e) {
    JENIS_TRANSAKSI_ID = $(this).data('id')
    clearForm('update')

    $('#jenis-transaksi-update-overlay').show()
    $.ajax({
        async: true,
        url: `${JENIS_TRANSAKSI_API_URL}by-id/${JENIS_TRANSAKSI_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#jenis-transaksi-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#jenis-transaksi-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".jenis-transaksi-delete-toggle", "click", function(e) {
    JENIS_TRANSAKSI_ID = $(this).data('id')
  })

  //submit form
  $('#jenis-transaksi-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#jenis-transaksi-create-button')
    
    let $form = $(this)
    let data = {
      nama:  $form.find( "input[name='nama']" ).val(),
      kode:  $form.find( "input[name='kode']" ).val(),
    }
    $.ajax({
      async: true,
      url: JENIS_TRANSAKSI_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#jenis-transaksi-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#jenis-transaksi-create-button', 'Submit')
        showSuccess(res.message)
        $('#jenis-transaksi-create-modal').modal('hide')
        jenis_transaksi_table.ajax.reload()
      }
    });
  })
  
  $('#jenis-transaksi-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#jenis-transaksi-update-button')

    let $form = $(this)
    let data = {
      id: JENIS_TRANSAKSI_ID,
      nama: $form.find( "input[name='nama']" ).val(),
      kode:  $form.find( "input[name='kode']" ).val(),
    }

    $.ajax({
        async: true,
        url: JENIS_TRANSAKSI_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#jenis-transaksi-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#jenis-transaksi-update-button', 'Submit')
          showSuccess(res.message)
          $('#jenis-transaksi-update-modal').modal('toggle')
          jenis_transaksi_table.ajax.reload()
        }
    });
  })

  $('#jenis-transaksi-delete-button').click(function (){
    startLoadingButton('#jenis-transaksi-delete-button')
    
    $.ajax({
        async: true,
        url: `${JENIS_TRANSAKSI_API_URL}${JENIS_TRANSAKSI_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#jenis-transaksi-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#jenis-transaksi-delete-button', 'Submit')
          $('#jenis-transaksi-delete-modal').modal('toggle')
          jenis_transaksi_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#jenis-transaksi-${type}-form`)
  $form.find( "input[name='nama']" ).val(data.nama)
  $form.find( "input[name='kode']" ).val(data.kode)
}

function clearForm(type){
  let $form = $(`#jenis-transaksi-${type}-form`)
  $form.find( "input[name='nama']" ).val("")
  $form.find( "input[name='kode']" ).val("")
}
