let UKURAN_ID

$(document).ready(function(){
  //render datatable
  let ukuran_table = $('#ukuran-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: VARIASI_UKURAN_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'id'
                  break
              case 1:
                  newObj.order_by = 'ukuran'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "id",
          },
          { 
            data: "ukuran",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary ukuran-update-toggle" data-id="${data}" data-toggle="modal" data-target="#ukuran-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger ukuran-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#ukuran-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#ukuran-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".ukuran-update-toggle", "click", function(e) {
    UKURAN_ID = $(this).data('id')
    clearForm('update')

    $('#ukuran-update-overlay').show()
    $.ajax({
        async: true,
        url: `${VARIASI_UKURAN_API_URL}by-id/${UKURAN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#ukuran-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#ukuran-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".ukuran-delete-toggle", "click", function(e) {
    UKURAN_ID = $(this).data('id')
  })

  //submit form
  $('#ukuran-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#ukuran-create-button')
    
    let $form = $(this)
    let data = {
      ukuran:  $form.find( "input[name='ukuran']" ).val(),
    }
    $.ajax({
      async: true,
      url: VARIASI_UKURAN_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#ukuran-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#ukuran-create-button', 'Submit')
        showSuccess(res.message)
        $('#ukuran-create-modal').modal('hide')
        ukuran_table.ajax.reload()
      }
    });
  })
  
  $('#ukuran-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#ukuran-update-button')

    let $form = $(this)
    let data = {
      id: UKURAN_ID,
      ukuran: $form.find( "input[name='ukuran']" ).val(),
    }

    $.ajax({
        async: true,
        url: VARIASI_UKURAN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#ukuran-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#ukuran-update-button', 'Submit')
          showSuccess(res.message)
          $('#ukuran-update-modal').modal('toggle')
          ukuran_table.ajax.reload()
        }
    });
  })

  $('#ukuran-delete-button').click(function (){
    startLoadingButton('#ukuran-delete-button')
    
    $.ajax({
        async: true,
        url: `${VARIASI_UKURAN_API_URL}${UKURAN_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#ukuran-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#ukuran-delete-button', 'Submit')
          $('#ukuran-delete-modal').modal('toggle')
          ukuran_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#ukuran-${type}-form`)
  $form.find( "input[name='ukuran']" ).val(data.ukuran)
}

function clearForm(type){
  let $form = $(`#ukuran-${type}-form`)
  $form.find( "input[name='ukuran']" ).val("")
}
