let TRANSFER_MASUK_ID

$(document).ready(function(){
  get_lokasi()

  //render datatable
  let transfer_masuk_table = $('#transfer-masuk-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: TRANSFER_MASUK_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'no_faktur'
                  break
              case 1:
                  newObj.order_by = 'nama_barang'
                  break
              case 2:
                  newObj.order_by = 'kode_barang'
                  break
              case 3:
                  newObj.order_by = 'qty'
                  break
              case 4:
                  newObj.order_by = 'lokasi'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "no_faktur",
          },
          { 
            data: "nama_barang",
          },
          { 
            data: "kode_barang",
          },
          { 
            data: "qty",
          },
          { 
            data: "lokasi",
          },
          {
            data: "created_at",
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-default transfer-masuk-detail-toggle" data-id="${data}" data-toggle="modal" data-target="#transfer-masuk-detail-modal" title="detail">
                <i class="fas fa-search"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });

  $('#transfer-masuk-item-create-field').select2({
    width: '100%',
    placeholder: "Cari Item..",
    selectOnClose: false,
    closeOnSelect:true,
    dropdownParent: $("#transfer-masuk-create-modal"),
    ajax: {
      url: `${STOK_API_URL}`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: function (params) {
        const lokasi = $('#transfer-masuk-lokasi-create-field').find(":selected").val()
        var query = {
          search: params.term,
          page_size: 10,
          page_number: 0,
          ordering: "ASC",
          order_by: "nama_barang",
        }
        if(lokasi){
          query.lokasi = lokasi
        }
  
        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (response) {
        let menu = response.data
        menu = menu.map(item => {
          item.text = `(${item.kode_barang}) ${item.nama_barang}`
          item.id = item.kode_barang
          return item
        })
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: menu
        };
      }
    }
  });
  $('#transfer-masuk-item-create-field').data('select2').$selection.css('height', '50px');
  $('#transfer-masuk-item-create-field').on("select2:selecting", function(e) {
    let item = e.params.args.data
    console.log("ITEM ", item)
    $('#transfer-masuk-kategori-create-field').val(item.kategori)
    $('#transfer-masuk-item-qty-create-field').val(item.stok)
  })

  $('#transfer-masuk-lokasi-create-field').change(function(){
    const val = $(this).val()
    const isDisabled = val ? false : true
    $('#transfer-masuk-item-create-field').val("").change()
    $('#transfer-masuk-kategori-create-field').val("")
    $('#transfer-masuk-item-qty-create-field').val("")
    $('#transfer-masuk-item-create-field').attr("disabled", isDisabled)
  })
  
  //button action click
  $("#transfer-masuk-create-toggle").click(function(e) {
    clearForm()
  })
  $("body").delegate(".transfer-masuk-detail-toggle", "click", function(e) {
    TRANSFER_MASUK_ID = $(this).data('id')

    $('#transfer-masuk-detail-overlay').show()
    $.ajax({
        async: true,
        url: `${TRANSFER_MASUK_API_URL}by-id/${TRANSFER_MASUK_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#transfer-masuk-detail-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#transfer-masuk-detail-overlay').hide()
          renderForm(response)
        }
    });
  })

  //submit form
  $('#transfer-masuk-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#transfer-masuk-create-button')
    
    let $form = $(this)
    let data = {
      no_faktur: $form.find( "input[name='no_faktur']" ).val(),
      lokasi_id: $('#transfer-masuk-lokasi-create-field').find(":selected").val(),
      kode_barang: $('#transfer-masuk-item-create-field').find(":selected").val(),
      qty: $form.find( "input[name='qty']" ).val(),
    }
    $.ajax({
      async: true,
      url: TRANSFER_MASUK_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#transfer-masuk-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#transfer-masuk-create-button', 'Submit')
        showSuccess(res.message)
        $('#transfer-masuk-create-modal').modal('hide')
        transfer_masuk_table.ajax.reload()
      }
    });
  })
})

function get_lokasi(){
  $.ajax({
    async: true,
    url: `${LOKASI_API_URL}list`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      render_lokasi(res.data)
    }
  });
}


function renderForm(data){
  let $form = $(`#transfer-masuk-detail-form`)
  $form.find( "input[name='no_faktur']" ).val(data.no_faktur)
  $form.find( "input[name='lokasi']" ).val(data.lokasi)
  $form.find( "input[name='nama_barang']" ).val(data.nama_barang)
  $form.find( "input[name='kode']" ).val(data.kode_barang)
  $form.find( "input[name='qty']" ).val(data.qty)
  $form.find( "input[name='tanggal']" ).val(data.tanggal)
}

function render_lokasi(data){
  let lokasi_html = `<option>--Pilih Lokasi--</option>`
  data.forEach(item => {
    let item_html = `<option value="${item.id}">${item.lokasi}</option>`
    lokasi_html += item_html
  });
  $('#transfer-masuk-lokasi-create-field').html(lokasi_html)
}

function clearForm(){
  let $form = $(`#transfer-masuk-create-form`)
  $form.find( "input[name='no_faktur']" ).val("")
  $form.find( "input[name='kategori']" ).val("")
  $form.find( "input[name='item_qty']" ).val("")
  $form.find( "input[name='qty']" ).val("")
  $('#transfer-masuk-item-create-field').val("").change()
}
