let UNIT_ID

$(document).ready(function(){
  //render datatable
  let unit_table = $('#unit-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: UNIT_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          newObj.ordering     = d.order[0].dir
          let column          = d.order[0].column
          switch (column) {
              case 0:
                  newObj.order_by = 'kode'
                  break
              case 1:
                  newObj.order_by = 'nama'
                  break
              case 2:
                  newObj.order_by = 'simbol'
                  break
          }
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      order: [[ 0, "asc" ]],
      columns: [
          { 
            data: "kode",
          },
          { 
            data: "nama",
          },
          { 
            data: "simbol",
          },
          {
            data: "created_at",
          },
          {
            data: "kode",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              return `<button class="btn btn-sm btn-primary unit-update-toggle" data-id="${data}" data-toggle="modal" data-target="#unit-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger unit-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#unit-delete-modal" title="delete">
                <i class="fas fa-trash"></i>
              </button>`
            },
            orderable: false
          }
      ]
  });
  
  //button action click
  $("#unit-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".unit-update-toggle", "click", function(e) {
    UNIT_ID = $(this).data('id')
    clearForm('update')

    $('#unit-update-overlay').show()
    $.ajax({
        async: true,
        url: `${UNIT_API_URL}by-id/${UNIT_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#unit-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#unit-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("body").delegate(".unit-delete-toggle", "click", function(e) {
    UNIT_ID = $(this).data('id')
  })

  //submit form
  $('#unit-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#unit-create-button')
    
    let $form = $(this)
    let data = {
      nama:  $form.find( "input[name='nama']" ).val(),
      simbol:  $form.find( "input[name='simbol']" ).val(),
    }
    $.ajax({
      async: true,
      url: UNIT_API_URL,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#unit-create-button', 'Submit')
      },
      success: function(res) {
        endLoadingButton('#unit-create-button', 'Submit')
        showSuccess(res.message)
        $('#unit-create-modal').modal('hide')
        unit_table.ajax.reload()
      }
    });
  })
  
  $('#unit-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#unit-update-button')

    let $form = $(this)
    let data = {
      id: UNIT_ID,
      nama: $form.find( "input[name='nama']" ).val(),
      simbol:  $form.find( "input[name='simbol']" ).val(),
    }

    $.ajax({
        async: true,
        url: UNIT_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#unit-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#unit-update-button', 'Submit')
          showSuccess(res.message)
          $('#unit-update-modal').modal('toggle')
          unit_table.ajax.reload()
        }
    });
  })

  $('#unit-delete-button').click(function (){
    startLoadingButton('#unit-delete-button')
    
    $.ajax({
        async: true,
        url: `${UNIT_API_URL}${UNIT_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#unit-delete-button', 'Submit')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#unit-delete-button', 'Submit')
          $('#unit-delete-modal').modal('toggle')
          unit_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#unit-${type}-form`)
  $form.find( "input[name='nama']" ).val(data.nama)
  $form.find( "input[name='simbol']" ).val(data.simbol)
}

function clearForm(type){
  let $form = $(`#unit-${type}-form`)
  $form.find( "input[name='nama']" ).val("")
  $form.find( "input[name='simbol']" ).val("")
}
