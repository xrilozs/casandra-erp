<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* Route */
#Home
$route['login']     = 'home/login';
$route['dashboard'] = 'home/dashboard';
#Operator
$route['operator']  = 'operator/page';
#Kategori
$route['kategori']  = 'kategori/page';
#Company
$route['lokasi']    = 'lokasi/page';
#Config
$route['suplier']   = 'suplier/page';
#Deposit
$route['unit']      = 'unit/page';
#variasi warna
$route['variasi-warna']   = 'variasi/variasi_warna';
#Variasi ukuran
$route['variasi-ukuran']  = 'variasi/variasi_ukuran';
#Item
$route['item']            = 'item/page';
#Pembelian
$route['pembelian']       = 'pembelian/page';
#Stok
$route['stok']            = 'stok/page';
#Transfer Masuk
$route['transfer-masuk']  = 'transfer_masuk/page';
#Transfer Keluar
$route['transfer-keluar'] = 'transfer_keluar/page';
#Jenis Konsumen
$route['jenis-konsumen']  = 'jenis_konsumen/page';
#Jenis Transaksi
$route['jenis-transaksi'] = 'jenis_transaksi/page';
#Konsumen
$route['konsumen']        = 'konsumen/page';