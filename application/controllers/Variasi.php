<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Variasi extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function variasi_warna(){    
    $data['page']     = "Variasi Warna";
    $data['version']  = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/variasi/warna');
		$this->load->view('layouts/main_footer', $data);
  }

  public function variasi_ukuran(){    
    $data['page']     = "Variasi Ukuran";
    $data['version']  = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/variasi/ukuran');
		$this->load->view('layouts/main_footer', $data);
  }
}