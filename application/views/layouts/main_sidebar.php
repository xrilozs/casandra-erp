  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?=base_url('dashboard');?>" class="brand-link">
      <img src="<?=base_url('assets/img/default-logo.png');?>" alt="Logo" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-bold"><?=COMPANY_NAME;?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" id="sidebar-dashboard-menu">
            <a href="<?=base_url('dashboard');?>" class="nav-link <?=$page == 'Dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-header">Data Barang</li>
          <li class="nav-item <?=in_array($page, array('Kategori', 'Lokasi', 'Suplier', 'Unit', 'Operator', 'Variasi Warna', 'Variasi Ukuran', 'Jenis Konsumen', 'Jenis Transaksi', 'Konsumen')) ? 'menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview pl-3">
              <li class="nav-item">
                <a href="<?=base_url('kategori');?>" class="nav-link <?=$page == 'Kategori' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-tags"></i>
                  <p>
                    Kategori
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('lokasi');?>" class="nav-link <?=$page == 'Lokasi' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-map"></i>
                  <p>
                    Lokasi
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('suplier');?>" class="nav-link <?=$page == 'Suplier' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-box"></i>
                  <p>
                    Suplier
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('unit');?>" class="nav-link <?=$page == 'Unit' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-weight-hanging"></i>
                  <p>
                    Unit
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('variasi-warna');?>" class="nav-link <?=$page == 'Variasi Warna' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-palette"></i>
                  <p>
                    Variasi Warna
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('variasi-ukuran');?>" class="nav-link <?=$page == 'Variasi Ukuran' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-ruler"></i>
                  <p>
                    Variasi Ukuran
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('jenis-konsumen');?>" class="nav-link <?=$page == 'Jenis Konsumen' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-th-large"></i>
                  <p>
                    Jenis Konsumen
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('jenis-transaksi');?>" class="nav-link <?=$page == 'Jenis Transaksi' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-th-large"></i>
                  <p>
                    Jenis Transaksi
                  </p>
                </a>
              </li>
              <li class="nav-item hidden-menu">
                <a href="<?=base_url('konsumen');?>" class="nav-link <?=$page == 'Konsumen' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-users"></i>
                  <p>
                    Konsumen
                  </p>
                </a>
              </li>
              <li class="nav-item hidden-menu">
                <a href="<?=base_url('operator');?>" class="nav-link <?=$page == 'Operator' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-users-cog"></i>
                  <p>
                    Operator
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?=in_array($page, array('Item', 'Stock', 'Transfer Masuk', 'Transfer Keluar')) ? 'menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Stok
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview pl-3">
              <li class="nav-item">
                <a href="<?=base_url('item');?>" class="nav-link <?=$page == 'Item' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-boxes"></i>
                  <p>
                    Item
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('stok');?>" class="nav-link <?=$page == 'Stok' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-pallet"></i>
                  <p>
                    Stock
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('transfer-masuk');?>" class="nav-link <?=$page == 'Transfer Masuk' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-sign-in-alt"></i>
                  <p>
                    Transfer Masuk
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('transfer-keluar');?>" class="nav-link <?=$page == 'Transfer Keluar' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>
                    Transfer Keluar
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">Transaksi</li>
          <li class="nav-item <?=$page == 'Pembelian' ? 'active' : '';?>">
            <a href="<?=base_url("pembelian");?>" class="nav-link">
              <i class="nav-icon fas fa-shopping-cart"></i>
              Pembelian
            </a>
          </li>
          <li class="nav-item <?=$page == 'Penjualan' ? 'active' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-hand-holding-usd"></i>
              Penjualan
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
