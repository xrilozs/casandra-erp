<footer class="main-footer" style="padding:12px; text-align:center;">
    <?=COMPANY_NAME;?> <b>ERP Version</b> 1.0
    <p class="mb-0" style="font-size:9px;">
      <strong>Copyright &copy; 2014-2020 AdminLTE.io. All rights reserved.</strong>
    </p>
</footer>
<!-- /.control-sidebar -->

  <!-- Common Modal -->
  <div class="modal fade" id="changePassword-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ganti Kata Sandi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="changePassword-form">
          <div class="form-group">
            <label>Kata Sandi Lama:</label>
            <input type="password" name="oldPassword" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Kata Sandi Baru:</label>
            <input type="password" name="newPassword" class="form-control" required>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success" id="changePassword-button">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/chart.js/Chart.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jszip/jszip.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/moment/moment.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=ASSETS;?>third-party/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS;?>third-party/adminlte/dist/js/adminlte.js"></script>
<!-- Select2 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>

<!-- Vitalets Datepicker -->
<!-- CodeMirror -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/codemirror.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/css/css.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/xml/xml.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<!-- Custom JS -->
<script>
  localStorage.setItem("current-page", "<?=$page;?>");
  localStorage.setItem("api-url", "<?=API_URL;?>");
  localStorage.setItem("erp-url", "<?=BASE_URL;?>");
</script>
<script src="<?=ASSETS;?>js/common.js?v=<?=$version;?>"></script>
<?= $page=="Home" ? '<script src="'.ASSETS.'js/home.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Dashboard" ? '<script src="'.ASSETS.'js/dashboard.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Operator" ? '<script src="'.ASSETS.'js/operator.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Kategori" ? '<script src="'.ASSETS.'js/kategori.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Lokasi" ? '<script src="'.ASSETS.'js/lokasi.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Suplier" ? '<script src="'.ASSETS.'js/suplier.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Unit" ? '<script src="'.ASSETS.'js/unit.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Variasi Warna" ? '<script src="'.ASSETS.'js/variasi_warna.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Variasi Ukuran" ? '<script src="'.ASSETS.'js/variasi_ukuran.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Item" ? '<script src="'.ASSETS.'js/item.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Pembelian" ? '<script src="'.ASSETS.'js/pembelian.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Stok" ? '<script src="'.ASSETS.'js/stok.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Transfer Masuk" ? '<script src="'.ASSETS.'js/transfer_masuk.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Transfer Keluar" ? '<script src="'.ASSETS.'js/transfer_keluar.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Jenis Konsumen" ? '<script src="'.ASSETS.'js/jenis_konsumen.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Jenis Transaksi" ? '<script src="'.ASSETS.'js/jenis_transaksi.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Konsumen" ? '<script src="'.ASSETS.'js/konsumen.js?v='.$version.'"></script>' : ''; ?>
</body>
</html>
