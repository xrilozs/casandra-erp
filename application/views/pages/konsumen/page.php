<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Konsumen</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-item active">Konsumen</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="konsumen-create-toggle" data-toggle="modal" data-target="#konsumen-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="konsumen-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>No Telepon</th>
                      <th>Email</th>
                      <th>Jenis Konsumen</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="konsumen-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Konsumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="konsumen-create-form">
        <div class="form-group">
          <label for="konsumen-username-create-field">Nama:</label>
          <input type="text" name="username" class="form-control" id="konsumen-username-create-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-phonr-create-field">No Telepon:</label>
          <input type="text" name="no_telpon" class="form-control" id="konsumen-phone-create-field" placeholder="No Telepon.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-email-create-field">Email:</label>
          <input type="email" name="email" class="form-control" id="konsumen-email-create-field" placeholder="email.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-dob-create-field">Tanggal Lahir:</label>
          <input type="text" name="dob" class="form-control datepicker" id="konsumen-dob-create-field" placeholder="Tanggal lahir.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-jenis-create-field">Jenis Konsumen:</label>
          <select name="jenis_konsumen" class="form-control jenis-konsumen-option" id="konsumen-jenis-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="konsumen-member-create-field">No Member:</label>
          <input type="text" name="no_member" class="form-control" id="konsumen-member-create-field" placeholder="no_ member.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="konsumen-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="konsumen-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="konsumen-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Konsumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="konsumen-update-form">
        <div class="form-group">
          <label for="konsumen-username-update-field">Nama:</label>
          <input type="text" name="username" class="form-control" id="konsumen-username-update-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-phonr-update-field">No Telepon:</label>
          <input type="text" name="no_telpon" class="form-control" id="konsumen-phone-update-field" placeholder="No Telepon.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-email-update-field">Email:</label>
          <input type="email" name="email" class="form-control" id="konsumen-email-update-field" placeholder="email.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-dob-update-field">Tanggal Lahir:</label>
          <input type="text" name="dob" class="form-control datepicker" id="konsumen-dob-update-field" placeholder="Tanggal lahir.." required>
        </div>
        <div class="form-group">
          <label for="konsumen-jenis-update-field">Jenis Konsumen:</label>
          <select name="jenis_konsumen" class="form-control jenis-konsumen-option" id="konsumen-jenis-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="konsumen-member-update-field">No Member:</label>
          <input type="text" name="no_member" class="form-control" id="konsumen-member-update-field" placeholder="no  member.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="konsumen-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="konsumen-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Konsumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus konsumen ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="konsumen-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

