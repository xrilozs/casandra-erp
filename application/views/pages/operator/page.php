<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Operator</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-item active">Operator</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="operator-create-toggle" data-toggle="modal" data-target="#operator-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="operator-datatable">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Role</th>
                      <th>Lokasi</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="operator-create-modal" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Operator</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="operator-create-form" autocomplete="off">          
        <div class="form-group">
          <label for="operator-username-create-field">Username:</label>
          <input type="text" name="username" class="form-control" id="operator-username-create-field" placeholder="username.." required>
        </div>
        <div class="form-group">
          <label for="operator-password-create-field">Password:</label>
          <input type="password" name="password" class="form-control" id="operator-password-create-field" placeholder="password.." autocomplete="new-password" required>
        </div>
        <div class="form-group">
          <label for="operator-role-create-field">Role:</label>
          <select class="form-control" name="role" id="operator-role-create-option" required>
            <option value="owner">Owner</option>
            <option value="manager">Manager</option>
            <option value="operator">Operator</option>
          </select>
        </div>
        <div class="form-group">
          <label for="operator-lokasi-create-field">Lokasi:</label>
          <select class="form-control lokasi-option" name="role" id="operator-lokasi-create-option" required>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="operator-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="operator-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay" id="operator-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Operator</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="operator-update-form">          
        <div class="form-group">
          <label for="operator-username-update-field">Username:</label>
          <input type="text" name="username" class="form-control" id="operator-username-update-field" placeholder="username.." required>
        </div>
        <div class="form-group">
          <label for="operator-role-update-field">Role:</label>
          <select class="form-control" name="role" id="operator-role-update-option" required>
            <option value="owner">Owner</option>
            <option value="manager">Manager</option>
            <option value="operator">Operator</option>
          </select>
        </div>
        <div class="form-group">
          <label for="operator-lokasi-update-field">Lokasi:</label>
          <select class="form-control lokasi-option" name="role" id="operator-lokasi-update-option" required>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="operator-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="operator-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Operator</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin menghapus operator ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="operator-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>
