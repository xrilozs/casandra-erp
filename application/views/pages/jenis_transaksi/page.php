<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Jenis Transaksi</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-item active">Jenis Transaksi</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="jenis-transaksi-create-toggle" data-toggle="modal" data-target="#jenis-transaksi-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="jenis-transaksi-datatable">
                  <thead>
                    <tr>
                      <th>Kode</th>
                      <th>Nama</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="jenis-transaksi-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Jenis Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="jenis-transaksi-create-form">
        <div class="form-group">
          <label for="jenis-transaksi-nama-create-field">Nama:</label>
          <input type="text" name="nama" class="form-control" id="jenis-transaksi-nama-create-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="jenis-transaksi-kode-create-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="jenis-transaksi-kode-create-field" placeholder="kode.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="jenis-transaksi-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="jenis-transaksi-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="jenis-transaksi-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Jenis Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="jenis-transaksi-update-form">
        <div class="form-group">
          <label for="jenis-transaksi-nama-update-field">Nama:</label>
          <input type="text" name="nama" class="form-control" id="jenis-transaksi-nama-update-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="jenis-transaksi-kode-update-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="jenis-transaksi-kode-update-field" placeholder="kode.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="jenis-transaksi-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="jenis-transaksi-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Jenis Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus jenis transaksi ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="jenis-transaksi-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

