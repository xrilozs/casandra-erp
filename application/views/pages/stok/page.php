<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Stok</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-stok"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-stok active">Stok</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="stok-datatable">
                  <thead>
                    <tr>
                      <th>kode</th>
                      <th>kode Master</th>
                      <th>Nama Barang</th>
                      <th>Lokasi</th>
                      <th>Suplier</th>
                      <th>Kategori</th>
                      <th>Unit</th>
                      <th>Warna</th>
                      <th>Ukuran</th>
                      <th>Stok</th>
                      <th>Harga Beli</th>
                      <th>Harga Jual</th>
                      <th>Acc</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="stok-detail-modal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="overlay" id="stok-detail-overlay">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Detail Stok</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="stok-detail-form">
          <div class="form-group">
            <label for="stok-nama-barang-detail-field">Nama Barang:</label>
            <input type="text" name="nama_barang" class="form-control" id="stok-nama-barang-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-kode-barang-detail-field">Kode:</label>
                <input type="text" name="kode_barang" class="form-control" id="stok-kode-barang-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-kode-master-detail-field">Kode Master:</label>
                <input type="text" name="kode_master" class="form-control" id="stok-kode-master-detail-field" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="stok-lokasi-detail-field">Lokasi:</label>
            <input type="text" name="lokasi" class="form-control" id="stok-lokasi-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-warna-detail-field">Warna:</label>
                <input type="text" name="warna" class="form-control" id="stok-warna-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-ukuran-detail-field">Ukuran:</label>
                <input type="text" name="ukuran" class="form-control" id="stok-ukuran-detail-field" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="stok-stok-detail-field">Stok:</label>
            <input type="text" name="stok" class="form-control" id="stok-stok-detail-field" readonly>
          </div>
          <div class="form-group">
            <label for="stok-suplier-detail-field">Suplier:</label>
            <input type="text" name="suplier" class="form-control" id="stok-suplier-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-kategori-detail-field">Kategori:</label>
                <input type="text" name="kategori" class="form-control" id="stok-kategori-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-unit-detail-field">Unit:</label>
                <input type="text" name="unit" class="form-control" id="stok-unit-detail-field" readonly>
              </div> 
            </div>
          </div>
          <div class="form-group">
            <label for="stok-rata-beli-detail-field">Rata Beli:</label>
            <input type="text" name="rata_beli" class="form-control" id="stok-rata-beli-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-beli-detail-field">Harga Beli:</label>
                <input type="text" name="harga_beli" class="form-control" id="stok-harga-beli-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-jual-detail-field">Harga Jual:</label>
                <input type="text" name="harga_jual" class="form-control" id="stok-harga-jual-detail-field" readonly>
              </div>
            </div>
          </div> 
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-grosir-detail-field">Harga Grosir:</label>
                <input type="text" name="harga_retail" class="form-control" id="stok-harga-grosir-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-pokok-detail-field">Harga Pokok:</label>
                <input type="text" name="harga_pokok" class="form-control" id="stok-harga-pokok-detail-field" readonly>
              </div>
            </div>
          </div> 
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-retail-detail-field">Harga Retail:</label>
                <input type="text" name="harga_retail" class="form-control" id="stok-harga-retail-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="stok-harga-reseller-detail-field">Harga Reseller:</label>
                <input type="text" name="harga_reseller" class="form-control" id="stok-harga-reseller-detail-field" readonly>
              </div>
            </div>
          </div> 
          <div class="form-group">
            <label for="stok-acc-detail-field">Acc:</label>
            <input type="text" name="acc" class="form-control" id="stok-acc-detail-field" readonly>
          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>
