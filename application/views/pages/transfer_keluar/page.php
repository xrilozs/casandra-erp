<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transfer Keluar</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-transfer-keluar"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-transfer-keluar active">Transfer Keluar</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="transfer-keluar-create-toggle" data-toggle="modal" data-target="#transfer-keluar-create-modal">
                  <i class="fas fa-plus"></i> Buat Transfer Keluar
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="transfer-keluar-datatable">
                  <thead>
                    <tr>
                      <th>No Faktur</th>
                      <th>Nama Barang</th>
                      <th>Kode</th>
                      <th>Qty</th>
                      <th>Lokasi</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="transfer-keluar-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="overlay" id="transfer-keluar-detail-overlay">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Detail Transfer Keluar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="transfer-keluar-detail-form">
          <div class="form-group">
            <label for="transfer-keluar-no-faktur-detail-field">No Faktur:</label>
            <input type="text" name="no_faktur" class="form-control" id="transfer-keluar-no-faktur-detail-field" readonly>
          </div>
          <div class="form-group">
            <label for="transfer-keluar-lokasi-detail-field">Lokasi:</label>
            <input type="text" name="lokasi" class="form-control" id="transfer-keluar-lokasi-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="transfer-keluar-model-detail-field">Nama Barang:</label>
                <input type="text" name="nama_barang" class="form-control" id="transfer-keluar-model-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="transfer-keluar-kode-detail-field">Kode:</label>
                <input type="text" name="kode" class="form-control" id="transfer-keluar-kode-detail-field" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="transfer-keluar-qty-detail-field">Qty Transfer:</label>
            <input type="text" name="qty" class="form-control" id="transfer-keluar-qty-detail-field" readonly>
          </div>
          <div class="form-group">
            <label for="transfer-keluar-tanggal-detail-field">Tanggal:</label>
            <input type="text" name="tanggal" class="form-control" id="transfer-keluar-tanggal-detail-field" readonly>
          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="transfer-keluar-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Transfer Keluar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="transfer-keluar-create-form">
        <div class="form-group">
          <label for="transfer-keluar-no-faktur-detail-field">No Faktur:</label>
          <input type="text" name="no_faktur" class="form-control" id="transfer-keluar-no-faktur-detail-field" placeholder="No Faktur.." required>
        </div>
        <div class="form-group">
          <label for="transfer-keluar-lokasi-create-field">Lokasi:</label>
          <select name="lokasi_id" class="form-control" id="transfer-keluar-lokasi-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="transfer-keluar-item-create-field">Item:</label>
          <select name="kode_barang" class="form-control" id="transfer-keluar-item-create-field" disabled required></select>
        </div>
        <div class="form-group">
          <label for="transfer-keluar-kategori-create-field">Kategori:</label>
          <input type="text" name="kategori" class="form-control" id="transfer-keluar-kategori-create-field" disabled>
        </div>
        <div class="form-group">
          <label for="transfer-keluar-item-qty-create-field">Qty Item:</label>
          <input type="text" name="item_qty" class="form-control" id="transfer-keluar-item-qty-create-field" disabled>
        </div>
        <div class="form-group">
          <label for="transfer-keluar-qty-create-field">Qty Transfer:</label>
          <input type="text" name="qty" class="form-control" id="transfer-keluar-qty-create-field" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        <button type="submit" class="btn btn-success" id="transfer-keluar-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
