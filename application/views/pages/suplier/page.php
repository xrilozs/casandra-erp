<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Suplier</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-item active">Suplier</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="suplier-create-toggle" data-toggle="modal" data-target="#suplier-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="suplier-datatable">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>No Telepon</th>
                      <th>Email</th>
                      <th>Deskripsi</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="suplier-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Suplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="suplier-create-form">
        <div class="form-group">
          <label for="suplier-nama-create-field">Nama:</label>
          <input type="text" name="nama" class="form-control" id="suplier-nama-create-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="suplier-no-telpon-create-field">No Telepon:</label>
          <input type="text" name="no_telpon" class="form-control" id="suplier-no-telpon-create-field" placeholder="No telepon.." required>
        </div>
        <div class="form-group">
          <label for="suplier-email-create-field">Email:</label>
          <input type="email" name="email" class="form-control" id="suplier-email-create-field" placeholder="email.." required>
        </div>
        <div class="form-group">
          <label for="suplier-keterangan-create-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="suplier-keterangan-create-field" placeholder="keterangan.." cols="30" rows="10"></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="suplier-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="suplier-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="suplier-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Suplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="suplier-update-form">
        <div class="form-group">
          <label for="suplier-nama-update-field">Nama:</label>
          <input type="text" name="nama" class="form-control" id="suplier-nama-update-field" placeholder="nama.." required>
        </div>
        <div class="form-group">
          <label for="suplier-no-telpon-update-field">No Telepon:</label>
          <input type="text" name="no_telpon" class="form-control" id="suplier-no-telpon-update-field" placeholder="No telepon.." required>
        </div>
        <div class="form-group">
          <label for="suplier-email-update-field">Email:</label>
          <input type="email" name="email" class="form-control" id="suplier-email-update-field" placeholder="email.." required>
        </div>
        <div class="form-group">
          <label for="suplier-keterangan-update-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="suplier-keterangan-update-field" placeholder="keterangan.." cols="30" rows="10"></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="suplier-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="suplier-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Suplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus suplier ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="suplier-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

