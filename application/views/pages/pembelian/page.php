<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pembelian</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-pembelian"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-pembelian active">Pembelian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="pembelian-create-toggle" data-toggle="modal" data-target="#pembelian-create-modal">
                  <i class="fas fa-plus"></i> Buat Pembelian
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="pembelian-datatable">
                  <thead>
                    <tr>
                      <th>No Faktur</th>
                      <th>Nama Barang</th>
                      <th>Harga</th>
                      <th>Qty</th>
                      <th>Diskon</th>
                      <th>PPN</th>
                      <th>Lokasi</th>
                      <th>Suplier</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="pembelian-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="overlay" id="pembelian-detail-overlay">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Detail Pembelian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="pembelian-detail-form">
          <div class="form-group">
            <label for="pembelian-no-faktur-detail-field">No Faktur:</label>
            <input type="text" name="no_faktur" class="form-control" id="pembelian-no-faktur-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-model-detail-field">Nama Barang:</label>
                <input type="text" name="model" class="form-control" id="pembelian-model-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-kode-detail-field">Kode:</label>
                <input type="text" name="kode" class="form-control" id="pembelian-kode-detail-field" readonly>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-harga-detail-field">Harga:</label>
                <input type="text" name="harga" class="form-control" id="pembelian-harga-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-harga-pokok-detail-field">Harga Pokok:</label>
                <input type="text" name="harga_pokok" class="form-control" id="pembelian-harga-pokok-detail-field" readonly>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="pembelian-qty-detail-field">Qty:</label>
            <input type="text" name="qty" class="form-control" id="pembelian-qty-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-diskon-detail-field">Diskon:</label>
                <input type="text" name="diskon" class="form-control" id="pembelian-diskon-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-ppn-detail-field">PPN:</label>
                <input type="text" name="ppn" class="form-control" id="pembelian-ppn-detail-field" readonly>
              </div> 
            </div>
          </div> 
          <div class="form-group">
            <label for="pembelian-tanggal-detail-field">Tanggal:</label>
            <input type="text" name="tanggal" class="form-control" id="pembelian-tanggal-detail-field" readonly>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-suplier-detail-field">Suplier:</label>
                <input type="text" name="suplier" class="form-control" id="pembelian-suplier-detail-field" readonly>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label for="pembelian-lokasi-detail-field">Lokasi:</label>
                <input type="text" name="lokasi" class="form-control" id="pembelian-lokasi-detail-field" readonly>
              </div>
            </div>
          </div> 
          <div class="form-group">
            <label for="pembelian-keterangan-detail-field">Keterangan:</label>
            <textarea name="keterangan" class="form-control" id="pembelian-keterangan-detail-field" readonly></textarea>
          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pembelian-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Pembelian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row mb-4 mx-2">
          <div class="col-4">
            <form id="pembelian-create-form">
            <div class="form-group">
              <label for="pembelian-no-faktur-detail-field">No Faktur:</label>
              <input type="text" name="no_faktur" class="form-control" id="pembelian-no-faktur-detail-field" placeholder="No Faktur.." required>
            </div>
            <div class="form-group">
              <label for="pembelian-keterangan-detail-field">Keterangan:</label>
              <textarea name="keterangan" class="form-control" id="pembelian-keterangan-detail-field" placeholder="Keterangan.."></textarea>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="pembelian-suplier-create-field">Suplier:</label>
              <select name="operator_id" class="form-control" id="pembelian-suplier-create-field" style="width:90%" required></select>
            </div>
            <div class="form-group">
              <label for="pembelian-lokasi-create-field">Lokasi:</label>
              <select name="lokasi_id" class="form-control" id="pembelian-lokasi-create-field" style="width:90%" required></select>
            </div>
          </div>
          <div class="col-4 p-2 border border-primary">
            <div class="form-group m-2">
              <label for="pembelian-lokasi-create-field">Item:</label>
              <select name="item" class="form-control" id="pembelian-detail-item-create-field"></select>
            </div>
            <div class="mx-2">
              <button type="button" class="btn btn-primary btn-block" id="pembelian-detail-item-button" disabled>Tambah</button>
            </div>
          </div>
        </div>
        <div class="row mx-2 mb-2">
          <div class="col-12 table-responsive">
            <table class="table table-bordered" id="pembelian-detail-table">
              <thead>
                <tr>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>PPN</th>
                  <th>Diskon</th>
                  <th>Harga Pokok</th>
                  <th>Hapus</th>
                </tr>
              </thead>
              <tbody id="pembelian-detail-row">
                <tr>
                  <td colspan="8"><p align="center">Belum ada item</p></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        <button type="submit" class="btn btn-success" id="pembelian-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
