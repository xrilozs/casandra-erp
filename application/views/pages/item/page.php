<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Item</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Home</a></li>
            <li class="breadcrumb-item active">Item</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="maste-create-toggle" data-toggle="modal" data-target="#master-create-modal">
                  <i class="fas fa-plus"></i> Tambah Master
                </button>&nbsp;
                <button type="button" class="btn btn-success" id="variant-create-toggle" data-toggle="modal" data-target="#variant-create-modal">
                  <i class="fas fa-plus"></i> Tambah Variasi
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="item-datatable">
                  <thead>
                    <tr>
                      <th>Tipe</th>
                      <th>Kode</th>
                      <th>Kode Master</th>
                      <th>Nama Barang</th>
                      <th>Dibuat tanggal</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="master-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Master</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="master-create-form">
        <div class="form-group">
          <label for="master-kode-create-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="master-kode-create-field" placeholder="Kode.." required>
        </div>
        <div class="form-group">
          <label for="master-model-create-field">Nama Barang:</label>
          <input type="text" name="model" class="form-control" id="master-model-create-field" placeholder="Nama Barang.." required>
        </div>
        <div class="form-group">
          <label for="master-satuan-create-field">Satuan:</label>
          <select name="kode_satuan" class="form-control satuan-option" id="master-satuan-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-kategori-create-field">Kategori:</label>
          <select name="kode_kategori" class="form-control kategori-option" id="master-kategori-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-suplier-create-field">Supplier:</label>
          <select name="id_suplier" class="form-control suplier-option" id="master-suplier-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-keterangan-create-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="master-keterangan-create-field"></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="master-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="variant-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Variasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="variant-create-form">
        <div class="form-group">
          <label for="variant-master-create-field">Master:</label>
          <select name="kode_master" class="form-control" id="variant-master-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="variant-kode-create-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="variant-kode-create-field" placeholder="Kode.." required>
        </div>
        <div class="form-group">
          <label for="variant-satuan-create-field">Satuan:</label>
          <input type="text" name="kode_satuan" class="form-control" id="variant-satuan-create-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-kategori-create-field">Kategori:</label>
          <input type="text" name="kode_kategori" class="form-control" id="variant-kategori-create-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-suplier-create-field">Supplier:</label>
          <input type="text" name="id_suplier" class="form-control" id="variant-suplier-create-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-variasi1-create-field">Variasi Warna:</label>
          <select name="kode_variasi1" class="form-control variasi1-option" id="variant-variasi1-create-field"></select>
        </div>
        <div class="form-group">
          <label for="variant-variasi2-create-field">Variasi Ukuran:</label>
          <select name="kode_variasi2" class="form-control variasi2-option" id="variant-variasi2-create-field"></select>
        </div>
        <div class="form-group">
          <label for="variant-keterangan-create-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="variant-keterangan-create-field" placeholder="Keterangan.."></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="variant-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="master-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="master-update-overlay">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Buat Master</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="master-update-form">
        <div class="form-group">
          <label for="master-kode-update-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="master-kode-update-field" placeholder="Kode.." disabled>
        </div>
        <div class="form-group">
          <label for="master-model-update-field">Nama Barang:</label>
          <input type="text" name="model" class="form-control" id="master-model-update-field" placeholder="Nama Barang.." required>
        </div>
        <div class="form-group">
          <label for="master-satuan-update-field">Satuan:</label>
          <select name="kode_satuan" class="form-control satuan-option" id="master-satuan-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-kategori-update-field">Kategori:</label>
          <select name="kode_kategori" class="form-control kategori-option" id="master-kategori-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-suplier-update-field">Supplier:</label>
          <select name="id_suplier" class="form-control suplier-option" id="master-suplier-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="master-keterangan-update-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="master-keterangan-update-field"></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="master-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="variant-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="variant-update-overlay">
        <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Buat Variasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="variant-update-form">
        <div class="form-group">
          <label for="variant-master-update-field">Master:</label>
          <input type="text" name="kode_master" class="form-control" id="variant-master-update-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-kode-update-field">Kode:</label>
          <input type="text" name="kode" class="form-control" id="variant-kode-update-field" placeholder="Kode.." disabled>
        </div>
        <div class="form-group">
          <label for="variant-satuan-update-field">Satuan:</label>
          <input type="text" name="kode_satuan" class="form-control satuan-option" id="variant-satuan-update-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-kategori-update-field">Kategori:</label>
          <input type="text" name="kode_kategori" class="form-control kategori-option" id="variant-kategori-update-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-suplier-update-field">Supplier:</label>
          <input type="text" name="id_suplier" class="form-control suplier-option" id="variant-suplier-update-field" disabled>
        </div>
        <div class="form-group">
          <label for="variant-variasi1-update-field">Variasi Warna:</label>
          <select name="kode_variasi1" class="form-control variasi1-option" id="variant-variasi1-update-field"></select>
        </div>
        <div class="form-group">
          <label for="variant-variasi2-update-field">Variasi Ukuran:</label>
          <select name="kode_variasi2" class="form-control variasi2-option" id="variant-variasi2-update-field"></select>
        </div>
        <div class="form-group">
          <label for="variant-keterangan-update-field">Keterangan:</label>
          <textarea name="keterangan" class="form-control" id="variant-keterangan-update-field" placeholder="Keterangan.."></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="variant-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="item-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Item</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus item ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="item-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

